@extends('layouts.admin')
@section('contenido')
    
 
   <div class="row">
       <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >
       <h3>Listado De Favoritos <a href="favorito/create"><button class="btn btn-success">Nuevo</button></a></h3>
       @include('chospasss.favorito.search')
       </div>

   </div>



   <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-condensed table-hover">
                         <thead>
                             <th>ID</th>
                             <th>NOMBRE</th>
                             <th>CATEGORIA</th>
                             <th>CONDICION</th>

                         </thead>
                         @foreach($favorito as $fav) 
                         
                         <tr>
                             <td>{{ $fav->idfavorito}}</td>
                             <td>{{ $fav->nombre}}</td>
                             <td>{{ $fav->categoria}}</td>
                             <td>{{ $fav->condicion}}</td> 


                             <td>
                             <a href="{{URL::action('FavoritoController@edit',$fav->idfavorito)}}"><button class="btn btn-info">Editar</button></a>
                             <a href="" data-target="#modal-delete-{{ $fav->idfavorito}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
                             </td>
                             
                         </tr>
                       @include('chospasss.favorito.modal')
                       @endforeach
                      </table>
                </div>
                {{$favorito->render()}}
         </div>

   </div>


@endsection

