@extends('layouts.admin')
@section('contenido')

    <div class="row">
        <div class ="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
          <h3>Editar Favorito:{{ $favorito->nombre}} </h3>
          @if (count($errors)>0) 
               <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach    
                  </ul>

               </div>
               @endif
      </div>
    </div>
               {!!Form::model($favorito,['method'=>'PATCH','route'=>['chospasss.favorito.update',$favorito->idfavorito],'files'=>'true'])!!}
               {{Form::token()}}


          <div class="row">
                 <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                      <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" name="nombre" required value="{{$favorito->nombre}}" class="form-control">
                      </div>
                 </div>
                 <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                       <div class="form-group">
                          <label> Categoria</label>
                          <select name="idcategoria" class="form-control">
                          @foreach ($categorias as $cat)
                              @if ($cat -> idcategoria == $favorito ->idcategoria )
                              <option value="{{$cat->idcategoria}}" selected>{{$cat->nombre}}</option>
                              @else
                              <option value="{{$cat->idcategoria}}">{{$cat->nombre}}</option>
                              @endif
                          @endforeach
                          </select>
                       </div>
                 </div>
         
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                      <div class="form-group">
                         <label for="descripcion">Descripcion</label>
                         <input type="text" name="descripcion" value="{{$favorito->descripcion}}" class="form-control" placeholder="Descripcion del articulo...">
                      </div>
                 </div>

                 <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">

                     <div class="form-group">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                        <button class="btn btn-danger"  type="reset">Cancelar</button>
                    </div>

                 </div>

            </div>


               {!!Form::close()!!}

@endsection