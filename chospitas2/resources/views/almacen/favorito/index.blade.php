@extends('layouts.admin')
@section('contenido')
    
 
   <div class="row">
       <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >
       <h3>Listado De Favoritos <a href="favorito/create"><button class="btn btn-success">Nuevo</button></a></h3>
       @include('almacen.favorito.search')
       </div>

   </div>



   <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-condensed table-hover">
                         <thead>
                             <th>ID</th>
                             <th>NOMBRE</th>
                             <th>CATEGORIA</th>
                             <th>ESTADO</th>

                         </thead>
                         @foreach($favorito as $fav) 
                         
                         <tr>
                             <td>{{ $fav->idfavorito}}</td>
                             <td>{{ $fav->nombre}}</td>
                             <td>{{ $fav->categoria}}</td>
                             <td>{{ $fav->estado}}</td> 


                             <td>
                             <a href=""><button class="btn btn-info">Editar</button></a>
                             <a href="" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
                             </td>
                             
                         </tr>

                       @endforeach
                      </table>
                </div>
                {{$favorito->render()}}
         </div>

   </div>


@endsection

