<?php

namespace chospitas\Http\Controllers;

use Illuminate\Http\Request;

use chospitas\Http\Requests;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;     /*para poder subir la imagen*/
use chospitas\Http\Requests\FavoritoFormRequest;
use chospitas\Favorito;
use DB;

class FavoritoController extends Controller
{
     public function __construct()
    {

    }
    public function index(Request $request)
    {
      if ($request)
         {
            $query=trim ($request->get('searchText'));
            $favorito =DB :: table('favorito as a')             //llama a la tabla favoritos
            ->join('categoria as c','a.idcategoria','=','c.idcategoria')   //consulta la categoria de favoritos en numero yla coonvierte en letras de la tabla categoria
            ->select('a.idfavorito','a.nombre','c.nombre as categoria','a.condicion')  //pide lo q mostrara en la tabla
            ->where('a.nombre','LIKE','%'.$query.'%')  //busca por el nombre del favorito
            ->orderBy ('a.idfavorito','desc')  //ordena descendentemente por el idfavorito
            ->paginate (7);  //pagina la tabla cada 7 filas

            //return view('almacen.favorito.index',["favorito"=>$favorito,"searchText"=>$query]);  //muestra todo el resultado en chospasss-> favorito-> index.blade.php
            return view('chospasss.favorito.index',["favorito"=>$favorito,"searchText"=>$query]);  //muestra todo el resultado en chospasss-> favorito-> index.blade.php


         }
    }


   ////////////////////////////// modulo para crear ////////////////////
   ////////////////////////////// modulo para crear ////////////////////
   ////////////////////////////// modulo para crear ////////////////////
    public function create()
    {
         $categorias=DB::table('categoria')->get();
          // return view ("almacen.favorito.create",["categorias"=>$categorias]);
           return view ("chospasss.favorito.create",["categorias"=>$categorias]);



    }


    public function store(FavoritoFormRequest $request)
    {
        $favorito = new Favorito;
        $favorito -> idcategoria= $request->get('idcategoria');
        $favorito -> nombre= $request->get('nombre');
        $favorito -> descripcion= $request->get('descripcion');
        $favorito -> condicion= 'activo';


        $favorito -> save();
        //return Redirect :: to('almacen/favorito');
        return Redirect :: to('chospasss/favorito');

    }
   
   //////////////////////// fin de modulo para crear ////////////////////
   //////////////////////// fin de modulo para crear ////////////////////
   //////////////////////// fin de modulo para crear ////////////////////


   ////////////////////////////// modulo para editar ////////////////////
   ////////////////////////////// modulo para editar ////////////////////
   ////////////////////////////// modulo para editar ////////////////////


   public function show($id)
    {
          return view("chospasss.favorito.show",["favorito"=>Favorito::findOrFail($id)]);
    }
    public function edit($id)
    {
        $favorito=Favorito::findOrFail($id);
        $categorias=DB::table('categoria')->get();
          return view("chospasss.favorito.edit",["favorito"=>$favorito,"categorias"=>$categorias]);
    }

    public function update(FavoritoFormRequest $request,$id )
    {
        $favorito = Favorito::findOrFail($id);
           
        $favorito -> idcategoria= $request->get('idcategoria');
        $favorito -> nombre= $request->get('nombre');
        $favorito -> descripcion= $request->get('descripcion');


           $favorito -> update();
           return Redirect :: to('chospasss/favorito');
    } 

       ////////////////////////////// fin  modulo para editar ////////////////////
       ////////////////////////////// fin  modulo para editar ////////////////////
       ////////////////////////////// fin  modulo para editar ////////////////////


   ////////////////////////////// modulo para eliminar ////////////////////
   ////////////////////////////// modulo para eliminar ////////////////////
   ////////////////////////////// modulo para eliminar ////////////////////

    public function destroy($id)
    {
           $favorito = Favorito::findOrFail($id);
           $favorito -> condicion= 'Inactivo';

        $favorito -> update();
        return Redirect :: to('chospasss/favorito');
    }

    ////////////////////////////// fin  modulo para eliminar ////////////////////
    ////////////////////////////// fin  modulo para eliminar ////////////////////
    ////////////////////////////// fin  modulo para eliminar ////////////////////

}
