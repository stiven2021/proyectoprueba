<?php

namespace chospitas\Http\Requests;

use chospitas\Http\Requests\Request;

class FavoritoFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'idcategoria'  => 'required',       
            'nombre'  => 'required | max:100', 
            'descripcion'  => 'max:512', 
            

        ];
    }
}
