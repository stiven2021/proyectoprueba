<?php

namespace chospitas;

use Illuminate\Database\Eloquent\Model;

class Favorito extends Model
{
    protected $table = 'favorito';
    protected $primaryKey = 'idfavorito';

    public $timestamps = false;


    protected $fillable = [
        'idcategoria',
        'nombre',
        'descripcion',
        'condicion',

    ];

    protected $guarded = [

    ];
}
