<?php

namespace chospitas;

use Illuminate\Database\Eloquent\Model;


//modelocategoria1.1
class Categoria extends Model
{
    protected $table = 'categoria';
    protected $primaryKey = 'idcategoria';

    public $timestamps = false;


    protected $fillable = [
        'nombre',
        'descripcion',
        'condicion',
    ];

    protected $guarded = [

    ];
}